$("#file-form").submit(function (e) {
    e.preventDefault();
    var data = new FormData($('#file-form').get(0));
    $.ajax({
        type: 'POST',
        url: $("#file-form").attr("form-url"),
        data: data,
        processData: false,
        contentType: false,
        
        success: function (response) {
            $("#file-form").trigger('reset');
            $("#success-div").show();

            var id = JSON.parse(response["id"]);
            // dirty hack so not to hardcode the url
            var url = $("#success-div").attr("results-url").replace(0, id);

            $("#success-div").append(
                `<p>Results are available at: <a href=${url}>${url}</a></p>`
            )},
        error: function (response) {
            alert(response.responseText);
        }
    })
})