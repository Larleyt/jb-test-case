from django.urls import path

from . import views

urlpatterns = [
    path('upload/', views.Predictor.as_view(), name='upload'),
    path('results/<int:pk>/', views.PredictionResults.as_view(), name='results'),
]
