import json
from pathlib import Path
from typing import List, BinaryIO

import numpy as np
import pandas as pd

from django.http import JsonResponse
from django.core.files.base import ContentFile
from rest_framework.exceptions import ValidationError
from rest_framework import generics
from rest_framework.parsers import MultiPartParser
from rest_framework.response import Response
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView

from .apps import EduModelConfig
from .data_tools import prepare_input_data
from .models import KnownTopicPredictions
from .serializers import KnownTopicPredictionsSerializer

from .constants import INPUT_FILE_COLUMNS


def validate_csv_header(file_columns: List, required_columns: List) -> None:
    if set(required_columns) - set(file_columns):
        raise ValidationError('The input file columns are different from what is expected. Check your file.')


def get_predictions(file_obj: BinaryIO) -> ContentFile:
    # read, validate, prepare a dataframe
    input_df = pd.read_csv(file_obj, index_col=0)
    validate_csv_header(input_df.columns, required_columns=INPUT_FILE_COLUMNS)
    input_df = prepare_input_data(input_df, EduModelConfig.tasks_info_df)

    # get actual predict
    probabilities = EduModelConfig.clf.predict_proba(input_df)
    
    # format output
    output_df = input_df[["user_id", "topic_id"]]
    # creating new column this way surprisingly gives a pandas' SettingWithCopyWarning, but nvm 
    output_df["prediction"] = np.round(probabilities[:, 0], decimals=2)

    json_grouped_by_user_id = json.dumps(
        output_df.set_index('user_id').groupby(level=0).apply(
            lambda x: x.to_dict(orient='records')
        ).to_dict()
    )

    result = ContentFile(
        json_grouped_by_user_id, 
        name=f"pred_{Path(file_obj.name).stem}.json"
    )
    return result


class Predictor(APIView):
    parser_classes = (MultiPartParser,)
    renderer_classes = [TemplateHTMLRenderer]
    template_name = "upload.html"

    def get(self, request):
        return Response(template_name=self.template_name)

    def post(self, request):
        # I'm doing all the form validation on backend due to API can possibly be accessed by url and not only at UI,
        # Also it is probably better to validate the input file inside of serializer, but we're not saving the file, only filename,
        # so I didn't want to mix the logic of input/output validation
        if not request.FILES:
            return JsonResponse({"error": "No file is sent"}, status=400)

        file_obj = request.FILES["file"]
        
        if not file_obj.content_type == "text/csv":
            return JsonResponse({"error": "The uploaded file has wrong content type"}, status=400)

        predictions = get_predictions(file_obj)
        
        data = {"input_filename": file_obj.name, "predictions_file": predictions}
        serializer = KnownTopicPredictionsSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)


class PredictionResults(generics.RetrieveAPIView):
    queryset = KnownTopicPredictions.objects.all()
    serializer_class = KnownTopicPredictionsSerializer
