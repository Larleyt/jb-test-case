import pathlib
import pickle

import pandas as pd 

from django.apps import AppConfig


class EduModelConfig(AppConfig):
    name = 'edu_model'
    
    APP_DIR = pathlib.Path(__file__).parent.absolute()
    DATA_DIR = APP_DIR.joinpath("data")
    MODEL_DIR = APP_DIR.joinpath("ml_models")

    TASKS_INFO_PATH = DATA_DIR.joinpath("tasks_info.csv")
    tasks_info_df = pd.read_csv(TASKS_INFO_PATH, index_col=0)

    LR_PRETRAINED_PATH = MODEL_DIR.joinpath("jb_edu_model_lr.bin")
    with open(str(LR_PRETRAINED_PATH), 'rb') as f:
        clf = pickle.load(f)  