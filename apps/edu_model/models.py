from django.db import models


class KnownTopicPredictions(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    input_filename = models.CharField(max_length=100, blank=False)
    predictions_file = models.FileField(upload_to='uploads/')