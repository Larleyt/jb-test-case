INPUT_FILE_COLUMNS = [
    'user_id', 
    'task_id', 
    'status', 
    'spent_seconds', 
    'failed_attempts'
]


# simple map: if user knows key-topic, then he knows value-topic(s)
ALSO_KNOWN_TOPICS = {
        335: [336],
        336: [333],
        399: [397, 404],
        402: [336],
        403: [335, 402],
        407: [406],
        408: [407],
        409: [406],
        418: [335],
        429: [406],
        431: [403],
        453: [409, 429],
        646: [633],
        1074: [615]
    }