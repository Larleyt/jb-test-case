import pandas as pd

from .constants import ALSO_KNOWN_TOPICS

def get_target_by_row(r):
    if r.topic_status == "marked_as_known" or getattr(r, f"topic_{r.topic_id}"):
        r.topic_status = 1
    else:
        r.topic_status = 0
    return r 
    
def get_known_topics_features_df(df, tasks_info_df):
    required_topics_df = pd.DataFrame(data=0, index=df.user_id.unique(), columns=["topic_{}".format(topic_id) for topic_id in tasks_info_df.topic_id.unique()])

    def find_connected_topics(x):
        """
        Disclaimer:
        the function is written this ugly way (using vars from outer scope) so that it can be used in apply(),
        no need to write like this on a usual basis, should be rewritten e.g. with itertools.partial submodule, 
        I've just got no time :)
        """
        if x.topic_id in ALSO_KNOWN_TOPICS and x.task_status == "solved":
            for req_topic_id in ALSO_KNOWN_TOPICS[x.topic_id]:
                required_topics_df.at[x.user_id, f"topic_{req_topic_id}"] = 1

    _ = df.apply(find_connected_topics, axis=1)
    return required_topics_df


def prepare_data(df, tasks_info_df, is_train=True):
    df["num_attempts"] = df["failed_attempts"] + 1
    df["num_attempts"] = df["num_attempts"].fillna(0)

    # new feature
    df["seconds_per_attempt"] = df[["num_attempts", "spent_seconds"]
        ].apply(lambda x: x.spent_seconds / x.num_attempts, axis=1)
    # fill NaNs with median for this task_id
    df['seconds_per_attempt'] = df['seconds_per_attempt'].fillna(df.groupby('task_id')['seconds_per_attempt'].transform('median'))

    # add task's topic info
    df = df.drop(columns=["topic_id"], errors='ignore')
    df = df.join(tasks_info_df.set_index("task_id"), on="task_id")

    # add aprior topic's knowledge
    additional_features_df = get_known_topics_features_df(df, tasks_info_df)
    df = df.join(additional_features_df, on="user_id", how="left")  

    # convert to binary or OHE
    df["task_status"] = df["task_status"].map(lambda x: 1 if x == "solved" else 0)
    # ..
    missing_cols_for_dummies = set(tasks_info_df.task_type.unique()) - set(df["task_type"].unique())
    task_types = pd.get_dummies(df["task_type"], prefix="task_type")
    for col in missing_cols_for_dummies:
        task_types[f"task_type_{col}"] = 0
    df = df.join(task_types)

    # drop unnecessary features
    df = df.drop(columns=["failed_attempts", "spent_seconds", "task_type"])
    
    if is_train:
        df = df.apply(get_target_by_row, axis=1)
    return df

def prepare_input_data(df, tasks_info_df):
    df = df.rename(columns={"status": "task_status"})
    df = prepare_data(df, tasks_info_df, is_train=False)
    return df
