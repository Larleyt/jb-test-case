from rest_framework import serializers
from .models import KnownTopicPredictions


class KnownTopicPredictionsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = KnownTopicPredictions
        fields = ['id', 'created_at', 'input_filename', 'predictions_file']

