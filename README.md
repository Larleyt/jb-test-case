# How to run the project

From the project root directory run:

```
$ pip install -r requirements.txt
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py runserver
```

The project has two available URLs:

1.  `/api/v1/edu_model/upload/` to upload a file to make predictions,
2. `/api/v1/edu_model/results/<int:pk>` to see results.


Note: The project was developed with `debug=True` and utilizes `/media/`, `/static/` accordingly, so some features might not work when `debug=False`.
